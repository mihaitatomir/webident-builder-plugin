# Introduction

This plugin extends the existing functionality of RainLab.Builder, RecordDetails component to support using custom translated slugs in other languages.

### Pre-requisites:
 - RainLab.Builder plugin

## Usage

Instead of using the original RecordDetails componebt from RaiLab.Builder, this one can be used.



## License

This plugin is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT) same as OctoberCMS platform.
